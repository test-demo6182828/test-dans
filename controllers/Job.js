import jwt from "jsonwebtoken";


export const getJobs = async(req, res) => {
    const page = req.query.page;
    const description = req.query.description;
    const location = req.query.location;
    const full_time = req.query.full_time;
    
    let parameter = '';
    if(page != undefined){
        parameter += (parameter == '') ? '?' : '&';
        parameter += 'page=' + page;
    }
    if(description != undefined){
        parameter += (parameter == '') ? '?' : '&';
        parameter += 'description=' + description.toLowerCase();
    }
    if(location != undefined){
        parameter += (parameter == '') ? '?' : '&';
        parameter += 'location=' + location.toLowerCase();
    }
    if(full_time != undefined){
        parameter += (parameter == '') ? '?' : '&';
        if(full_time == true){
            parameter += 'type=full time';
        }else{
            parameter += 'type=part time';
        }
    }

    let url = 'http://dev3.dansmultipro.co.id/api/recruitment/positions.json'+parameter;
    console.log(url);

    const resp = await fetch(url);
	const result = await resp.json();

    res.json(result);
}


export const getJobDetail = async(req, res) => {
    let id = req.params.id;

    let url = 'http://dev3.dansmultipro.co.id/api/recruitment/positions/'+id;
    console.log(url);

    const resp = await fetch(url);
	const result = await resp.json();

    res.json(result);
}